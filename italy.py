# -*- coding: utf-8 -*-
import plotly.graph_objects as go
import pandas as pd
from numpy import identity, append
import itertools


def per_diem(regions,data,label):

    lp = label+"_per_diem"
    for r in regions:
        prev = 0
        for i in data[r].index:
            data[r].at[i,lp] = data[r][label].loc[i] - prev
            prev = data[r][label].loc[i]

def c_totale(regions,bs,days,dfs):

    d = dict()
    d['data'] = days

    for b in bs:
        d[b] = [ sum([dfs[r][dfs[r].data == day][b].values for r in regions])[0] for day in days]

    return pd.DataFrame(data=d)



def build_figure_it(filename):

    df = pd.read_json(filename).sort_values('data')
    df['tamponi_per_diem'] = df.apply(lambda row: row.tamponi, axis = 1)
    df['dimessi_guariti_per_diem'] = df.apply(lambda row: row.dimessi_guariti, axis = 1)
    df['deceduti_per_diem'] = df.apply(lambda row: row.deceduti, axis = 1)

    #regions = ['Umbria','Sardegna','Lazio','Sicilia','Lombardia']
    regions = df.denominazione_regione.unique()
    days = df.data.unique()
    bs = ['nuovi_positivi','tamponi','tamponi_per_diem','terapia_intensiva','variazione_totale_positivi','totale_casi','totale_positivi','dimessi_guariti','dimessi_guariti_per_diem','deceduti','deceduti_per_diem']
    labels = ["Nuovi Positivi","Tamponi","Tamponi per diem","Terapia Intensiva","Variazione totale positivi","Totale casi","Totale positivi","Dimessi guariti","Dimessi guariti per diem", "Deceduti", "Deceduti per diem"]

    dfs = dict( (r, df[df.denominazione_regione == r]) for r in regions )
    per_diem(regions,dfs,'tamponi')
    per_diem(regions,dfs,'dimessi_guariti')
    per_diem(regions,dfs,'deceduti')

    dfs['totale'] = c_totale(regions, bs, days, dfs)
    regions = append(regions, ['totale'])
    nr = len(regions)


    fig = go.Figure()
    fig.layout.template="presentation"
    fig.layout.title=labels[0]


    n = len(bs)
    a = identity(n,dtype=bool).tolist()
    visibles = [list(itertools.chain.from_iterable(itertools.repeat(x, nr) for x in i)) for i in a ]

    show = True
    for y in bs:
        for r in regions:
            fig.add_trace(go.Scattergl(
                x=dfs[r]['data'],
                y=dfs[r][y],
                mode='lines+markers',
                name = r,
                visible = show,
                )
                )
        show = False

    fig.layout.update(
    updatemenus = [
        go.layout.Updatemenu(
            type = "buttons",active=0, direction = "right",xanchor='left',yanchor='top', y=-0.1,
            buttons = list(
            [
                dict(
                    label = bs[i], method = "update",
                    args = [{"visible": visibles[i]},{"title": labels[i]} ]
                )
                for i in range(n)]
            )
        )
    ]
    )
    return fig

if __name__ == '__main__':
    fig = build_figure_it("dpc-covid19-ita-regioni.json")
    fig.show()
