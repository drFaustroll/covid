these are simple visualization of covid data from two sources

+ john hopkins for world data
+ italy from their ministry

while some data is hosted in these repo to work properly you may want to copy the respective scripts in the original sources for
data.

see requirements.txt for packages required to work

+ world data

```
git clone  https://github.com/CSSEGISandData/COVID-19.git
cd COVID-19/csse_covid_19_data/csse_covid_19_time_series

```

cp world.py and world-dash.py in this folder. to run

```
python3 ./world-dash.py
```

follow instructions


+ italy

```
git clone https://github.com/pcm-dpc/COVID-19.git it-covid-19
cd it-covid-19/dati-json
```


cp italy.py and italy-dash.py in this folder. to run
```
python3 ./italy-dash.py
```

majority of this was instigated due to boredom, parts of it were written after a bbq which is reflected in the quality of the code.
