#!/home/drFaustroll/venv/covid/bin/python3
# -*- coding: utf-8 -*-
from world import build_figure
from waitress import serve
from italy import build_figure_it

import dash
import dash_core_components as dcc
import dash_html_components as html


sc = ['China','Spain','Italy','Denmark','US','United Kingdom','Switzerland','Germany','France']
fig = build_figure(confirmed="time_series_covid19_confirmed_global.csv",death="time_series_covid19_deaths_global.csv",recovered="time_series_covid19_recovered_global.csv",select_countries=sc)

fig_it = build_figure_it("dpc-covid19-ita-regioni.json")
app = dash.Dash(
    __name__,
    external_stylesheets=[
        'https://codepen.io/chriddyp/pen/bWLwgP.css'
    ]
)
app.layout = html.Div([
                html.Div([
                    html.Div([
                        html.H2('World, scroll down for Italy'),
                            dcc.Graph(figure=fig,
                                style={'height': 800},
                            )
                    ], className="twelve columns"),
                ], className="row"),
                html.Div([
                    html.Div([
                        html.H2('Italy by region'),
                            dcc.Graph(figure=fig_it,
                                style={'height': 800},
                            )
                    ], className="twelve columns"),
                ], className="row")
            ])

if __name__ == '__main__':
#    app.run_server(debug=False)
     serve(app.server,port=8050,threads=10)
