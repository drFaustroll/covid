# -*- coding: utf-8 -*-
import plotly.graph_objects as go
import pandas as pd
from numpy import identity
import itertools
import copy
from datetime import datetime


def active(sc,dc,dd,dr):

    d = copy.deepcopy(dc)
    for s in sc:
        for i in dc[s].index[4:]:
            d[s][i] = dc[s][i] - dd[s][i] - dr[s][i]
    return d

def per_diem(sc,data):

    d = copy.deepcopy(data)
    for s in sc:
        prev = 0
        for i in data[s].index[4:]:
            d[s][i] = data[s][i] - prev
            prev = data[s][i]
    return d

def build_figure(confirmed,death,recovered,select_countries):

    dfc = pd.read_csv(confirmed)
    dfd = pd.read_csv(death)
    dfr = pd.read_csv(recovered)
    countries = dfc['Country/Region'].unique()

    dc = dict( (c,dfc[ dfc['Country/Region'] == c ].sum()) for c in select_countries)
    dd = dict( (c,dfd[ dfd['Country/Region'] == c ].sum()) for c in select_countries)
    dr = dict( (c,dfr[ dfr['Country/Region'] == c ].sum()) for c in select_countries)
    bs = ['confirmed','confirmed_per_diem','death','death_per_diem','recovered','recovered_per_diem','active']
    labels = ["Confirmed","Confirmed per diem","Death","Death per diem","Recovered","Recovered per diem","Active"]
    idays = dc[select_countries[0]].index[4:]
    ddays = [datetime.strptime(x, '%m/%d/%y') for x in list(idays) ]

    dcp = per_diem(select_countries,dc)
    ddp = per_diem(select_countries,dd)
    drp = per_diem(select_countries,dr)
    da = active(select_countries,dc,dd,dr)
    #per_diem(select_countries,dfd,'death_per_diem')
    data = dict()
    data['confirmed'] =  dc
    data['confirmed_per_diem'] =  dcp
    data['death'] = dd
    data['death_per_diem']  = ddp
    data['recovered'] = dr
    data['recovered_per_diem'] = drp
    data['active'] = da

    fig = go.Figure()
    fig.layout.template="presentation"
    fig.layout.title=labels[0]


    n = len(bs)
    nc = len(select_countries)
    a = identity(n,dtype=bool).tolist()
    visibles = [list(itertools.chain.from_iterable(itertools.repeat(x, nc) for x in i)) for i in a ]

    show = True
    for y in bs:
        d = data[y]
        for c in select_countries:
            fig.add_trace(go.Scatter(
                x=ddays,
                y=d[c][4:],
                mode='lines+markers',
                name = c,
                visible = show,
                )
                )
        show = False

    fig.layout.update(
    updatemenus = [
        go.layout.Updatemenu(
            type = "buttons",active=0, direction = "right",xanchor='left',yanchor='top', y=-0.1,
            buttons = [
                dict(
                    label = bs[i], method = "update",
                    args = [{"visible": visibles[i]},{"title": labels[i]} ]
                )
                for i in range(n)
                ] + [
                              #dict(label="Play",
                                        #method="animate",
                                        #args=[None]),
                                    #dict(label="Pause",
                                          #method="animate",
                                          #args=[[None], {"frame": {"duration": 0, "redraw": False},
                                  #"mode": "immediate",
                                  #"transition": {"duration": 0}}])
                    ]
        )
    ]
    )
    fig.frames=[
            go.Frame(
            data=[go.Scatter(
                x=ddays[:i+1],
                y=dc[c][4:4+i+1],
                  mode="lines+markers",
                  name = c,
                  marker=dict(size=10))
                  for c in select_countries])
            for i in range(1,len(idays))]

    return fig

if __name__ == '__main__':
    sc = ['China','Iran','Israel','Norway','Spain','Italy','US','United Kingdom','Switzerland','Germany','France']
    fig = build_figure(confirmed="time_series_covid19_confirmed_global.csv",death="time_series_covid19_deaths_global.csv",recovered="time_series_covid19_recovered_global.csv",select_countries=sc)
    fig.show()
