# -*- coding: utf-8 -*-
import plotly.graph_objects as go
import pandas as pd

def build_figure_uk(uk):

    df = pd.read_csv(uk,sep=";")

    fig = go.Figure()
    fig.layout.template="presentation"
    fig.layout.title="registered deaths in England and Wales per week"

    for i in range(1,8,1):
      fig.add_trace(go.Scatter(
        x=df.loc[0][1:],
        y=df.loc[i][1:],
        mode='lines+markers',
        name = df.loc[i][0]
        ))

    return fig

if __name__ == '__main__':
    fig = build_figure_uk(uk="uk.csv")
    fig.show()
