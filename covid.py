#!/home/drFaustroll/venv/covid/bin/python3
# -*- coding: utf-8 -*-
import dash
import dash_bootstrap_components as dbc
import dash_core_components as dcc
import dash_html_components as html

from world import build_figure
from waitress import serve
from italy import build_figure_it
from uk import build_figure_uk

app = dash.Dash(__name__, external_stylesheets=[dbc.themes.DARKLY])
app.title = 'covid visualization'
sc = ['China','Spain','Italy','Sweden','Finland','Denmark','Austria','Belgium','Israel','Norway','US','United Kingdom','Switzerland','Germany','France','Romania']
fig = build_figure(confirmed="time_series_covid19_confirmed_global.csv",death="time_series_covid19_deaths_global.csv",recovered="time_series_covid19_recovered_global.csv",select_countries=sc)

fig_it = build_figure_it("dpc-covid19-ita-regioni.json")
fig_uk = build_figure_uk("uk.csv")

app.layout = html.Div([
    dbc.Tabs([
    dbc.Tab(label='Italy', children=[
                        html.H2('Italy by region'),
                        dbc.Row([dbc.Col(html.Div(
                            dcc.Graph(figure=fig_it,style={'height': '80vh','width':'100vw'},
                                )))])
                    ]),
    dbc.Tab(label='World', children=[
                        html.H2('World'),
                        dbc.Row([dbc.Col(html.Div(
                            dcc.Graph(figure=fig,style={'height': '80vh','width':'100vw'},
                                )))])
                    ]),
    dbc.Tab(label='UK', children=[
                        html.H2('UK'),
                        dbc.Row([dbc.Col(html.Div(
                            dcc.Graph(figure=fig_uk,style={'height': '80vh','width':'100vw'},
                                )))])
                    ]),
])
    ])

if __name__ == '__main__':
    serve(app.server,port=8050,threads=10)
