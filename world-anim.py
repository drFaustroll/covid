import plotly.graph_objects as go
from datetime import datetime

import numpy as np
import pandas as pd

select_countries = ['China','Spain','Italy','US','United Kingdom','Switzerland','Germany','France']
url = "time_series_covid19_confirmed_global.csv"

dataset = pd.read_csv(url)
countries = dataset['Country/Region'].unique()
dc = dict( (c,dataset[ dataset['Country/Region'] == c ].sum()) for c in select_countries)
idays = dc[select_countries[0]].index[4:]
ddays = [datetime.strptime(x, '%m/%d/%y') for x in list(idays) ]
maxy = max([dc[c][4:].max() for c in select_countries])

# Create figure
fig = go.Figure(
        data=[go.Scatter(x=ddays, y=dc[c][4:],
                     mode="lines+markers",
                     name = c,
                     line=dict(width=2)) for c in select_countries],
    layout=go.Layout(
        xaxis=dict(range=[ddays[0], ddays[-1]], autorange=False, zeroline=False),
        yaxis=dict(range=[0, maxy], autorange=False, zeroline=False),
        title_text="confirmed cases",
        template='presentation',
        updatemenus=[dict(type="buttons",
                          buttons=[
                              dict(label="Play",
                                        method="animate",
                                        args=[None]),
                                    dict(label="Pause",
                                          method="animate",
                                          args=[[None], {"frame": {"duration": 0, "redraw": False},
                                  "mode": "immediate",
                                  "transition": {"duration": 0}}])
                                          ])],
    #sliders = [dict(steps= [dict(method= 'animate',
                           #args= [[ 'frame{}'.format(k) ],
                                  #dict(mode= 'immediate',
                                  #frame= dict( duration=100, redraw= True ),
                                           #transition=dict( duration= 0)
                                          #)
                                    #],
                            #label='{:d}'.format(k)
                             #) for k in range(len(idays))],
                #transition= dict(duration= 0 ),
                #x=0.0,#slider starting position
                #y =-0.1,
                #currentvalue=dict(font=dict(size=12),
                                  #prefix='Date: ',
                                  #visible=True,
                                  #xanchor= 'center'),
                #len=1.0)
           #]
                          ),
    frames=[go.Frame(
            data=[go.Scatter(
                x=ddays[:i+1],
                y=dc[c][4:4+i+1],
                  mode="lines+markers",
                  name = c,
                  marker=dict(size=10))
                  for c in select_countries])
            for i in range(1,len(idays))],

)
fig.show()
